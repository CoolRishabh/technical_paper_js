1. **for loop**:
   The `for` loop is a common loop used to iterate over a range of values.

   ```javascript
   for (let i = 0; i < 5; i++) {
       console.log(i);
   }
   // Output: 0, 1, 2, 3, 4
   ```

2. **forEach loop**:
   The `forEach` method is used to iterate over arrays and is more concise than a traditional `for` loop.

   ```javascript
   const numbers = [1, 2, 3, 4, 5];
   numbers.forEach(number => {
       console.log(number);
   });
   // Output: 1, 2, 3, 4, 5
   ```

3. **for in loop**:
   The `for..in` loop iterates over the enumerable properties of an object.

   ```javascript
   const person = {
       name: 'John',
       age: 30,
       city: 'New York'
   };

   for (let key in person) {
       console.log(`${key}: ${person[key]}`);
   }
   // Output: name: John, age: 30, city: New York
   ```

4. **for of loop**:
   The `for..of` loop is used to iterate over iterable objects like arrays, strings, maps, sets, etc.

   ```javascript
   const fruits = ['apple', 'banana', 'orange'];

   for (let fruit of fruits) {
       console.log(fruit);
   }
   // Output: apple, banana, orange
   ```

5. **while loop**:
   The `while` loop executes a block of code while a specified condition is true.

   ```javascript
   let count = 0;
   while (count < 5) {
       console.log(count);
       count++;
   }
   // Output: 0, 1, 2, 3, 4
   ```
6. **do-while loop**:
  Similar to a `while` loop, but the condition is evaluated after the block of code is executed, meaning it will always execute at least once.

  ```javascript
  let i = 0;
  do {
    console.log(count);
    count++;
  } while (count < 5);
  // Output: 0, 1, 2, 3, 4
  ```  
