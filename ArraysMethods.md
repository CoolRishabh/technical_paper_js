# Arrays Methods:

### Basic: 

1. #### Array.pop():
   Removes the last element from an array and returns that element.
   
   ***Mutable***
    ```
    let num =[1,2,3,4];
    console.log(num.pop()) // OUTPUT : 4
    console.log(num)       // OUTPUT : [1,2,3]
    ```

2. #### Array.push():
    Adds one or more elements to the end of an array and returns the new length of the array.
    
    ***Mutable***
    ```javascript
    let num =[0,1,2,3,4];
    console.log(num.push(5)) // OUTPUT : 6  //length
    console.log(num)       // OUTPUT : [0,1,2,3,4,5]
    ```

3. #### Array.concat():
    Returns a new array comprised of the array on which it is called, followed by the arrays passed as arguments.

    ***Immutable***
```javascript
    let arr1 = [1, 2];
    let arr2 = [3, 4];
    let newArr = arr1.concat(arr2); // returns [1, 2, 3, 4]
    console.log(newArr);
```

4. #### Array.slice():
   Returns a shallow copy of a portion of an array into a new array object selected from start to end.

    ***Immutable***
    ```javascript
    let arr = [1, 2, 3, 4, 5];
    let newArr = arr.slice(1, 3); // Returns [2, 3]
    console.log(newArr);
    ```

5. #### Array.splice():
   Changes the contents of an array by removing or replacing existing elements and/or adding new elements in place.

    ***Mutable***
    ```javascript
    let arr = [1, 2, 3, 4, 5];
    arr.splice(2, 1); // Removes one element at index 2
    console.log(arr); // Output: [1, 2, 4, 5]
    ```

6. #### Array.join():
    Joins all elements of an array into a string.

    ***Immutable***
    ```javascript
    let arr = ["Hello", "World"];
    let str = arr.join(", "); // Returns "Hello, World"
    console.log(str);
    ```

7. #### Array.flat():
    creates a new array with all sub-array elements concatenated into it recursively up to the specified depth.

    ***Immutable***
    ```javascript
    const arr1 = [0, 1, 2, [3, 4]];

    console.log(arr1.flat());
    // expected output: Array [0, 1, 2, 3, 4]
    ```

### Finding:
1. Array.find : 

   returns the first element in the provided array that satisfies the provided testing function. If no values satisfy the testing function, undefined is returned.

2. Array.indexOf: 
   
   Returns the first index at which a given element can be found in the array, or -1 if it is not present.

3. Array.includes: 
   
   Determines whether an array contains a certain element, returning true or false.

4. Array.findIndex:
   
   Returns the index of the first element in the array that satisfies the provided testing function.

### Higher Order Functions:
These methods accept callback functions and operate on each element of the array:

1. Array.forEach: 
   
   Executes a provided function once for each array element.

2. Array.filter: 
   
   Creates a new array with all elements that pass the test implemented by the provided function.

3. Array.map: 
   
   Creates a new array populated with the results of calling a provided function on every element in the calling array.

4. Array.reduce:
   
   Applies a function against an accumulator and each element in the array to reduce it to a single value.

5. Array.sort: 
   
   Sorts the elements of an array in place and returns the sorted array.

6. Array methods chaining:

    chaining multiple array methods together to perform complex operations in a concise manner.

    ```javascript
    let numbers = [1, 2, 3, 4, 5];

    let result = numbers
    .filter(num => num % 2 === 0) // Filter even numbers
    .map(num => num * 2) // Double each remaining number
    .reduce((acc, curr) => acc + curr, 0); // Sum all numbers

    console.log(result); // Output: 12 (2 * 2 + 4 * 2)
    ```