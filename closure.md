# Closure

Closure is a powerful and fundamental concept. It refers to the combination of a function and the lexical environment within which that function was declared. This combination allows the function to retain access to variables from its containing scope even after the scope has closed or the outer function has finished executing.

Here's a breakdown with an example:

```javascript
function outerFunction() {
  let outerVariable = 'I am from outerFunction';
  
  function innerFunction() {
    console.log(outerVariable); // Accessing outerVariable from the outer scope
  }
  
  return innerFunction;
}

const tempFunc = outerFunction(); // outerFunction has finished executing, but innerFunction still retains access to outerVariable

tempFunc(); // Output: "I am from outerFunction"
```

In this example, `innerFunction` is defined inside `outerFunction`. Despite `outerFunction` completing execution, `innerFunction` still has access to the `outerVariable`. This is because `innerFunction` forms a closure over the `outerVariable` and retains a reference to it.

Uses of closures in JavaScript:

1. **Encapsulation**: Closures can be used to encapsulate variables and functions, limiting their scope and exposure. This helps in creating modules and maintaining clean code.

2. **Data Privacy**: By encapsulating variables within closures, you can create private variables that are inaccessible from outside the closure, enhancing data privacy and security.

3. **Callbacks**: Closures are commonly used in asynchronous operations and event handling, where a function is passed as a callback and retains access to its surrounding state.

4. **Partial Application and Currying**: Closures can be used to create functions with partially applied arguments, enabling currying and functional programming patterns.

5. **Memoization**: Closures can be utilized to store and retrieve the results of expensive function calls, improving performance through memoization.

Closure provide a powerful mechanism for managing scope and maintaining state within your programs.