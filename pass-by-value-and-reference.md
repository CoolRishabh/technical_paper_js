## Pass by Value in JavaScript:
When you pass primitive data types (like numbers, strings, booleans) to a function in JavaScript, they are passed by value.

```javascript
function modifyValue(x) {
    x = x * 2;
    console.log("Inside modifyValue function:", x);
}

let num = 5;
console.log("Before calling modifyValue function:", num);
modifyValue(num);
console.log("After calling modifyValue function:", num);
```

Output:
```
Before calling modifyValue function: 5
Inside modifyValue function: 10
After calling modifyValue function: 5
```

As in the previous example, the value of `num` remains unchanged because it's passed by value.

## Pass by Reference in JavaScript:
When you pass objects or arrays to a function in JavaScript, they are passed by value, but the value is a reference to the object or array. Therefore, changes made to the object or array inside the function affect the original object or array.

```javascript
function modifyReference(obj) {
    obj.name = "John Doe";
    console.log("Inside modifyReference function:", obj);
}

let person = { name: "Alice" };
console.log("Before calling modifyReference function:", person);
modifyReference(person);
console.log("After calling modifyReference function:", person);
```

Output:
```
Before calling modifyReference function: { name: 'Alice' }
Inside modifyReference function: { name: 'John Doe' }
After calling modifyReference function: { name: 'John Doe' }
```

Here, the `person` object is passed by value, but since the value is a reference to the object, changes made inside the `modifyReference` function are reflected in the original `person` object.