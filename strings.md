# String in JavaScript

1. **charAt(index)**: Returns the character at the specified index in a string.
   ```javascript
   const str = "Hello";
   const char = str.charAt(1); // Returns 'e'
   ```

2. **concat(str1, str2, ...)**: Combines the text of two or more strings and returns a new string.
   ```javascript
   const str1 = "Hello";
   const str2 = " world!";
   const result = str1.concat(str2); // Returns 'Hello world!'
   ```

3. **indexOf(searchValue, startIndex)**: Returns the index within the calling String object of the first occurrence of the specified value, starting the search at fromIndex. Returns -1 if the value is not found.
   ```javascript
   const str = "Hello world";
   const index = str.indexOf("world"); // Returns 6
   const index1 = str.indexOf("apple"); // return -1
   ```

4. **slice(startIndex, endIndex)**: Extracts a section of a string and returns it as a new string.
   ```javascript
   const str = "Hello world";
   const sliced = str.slice(6, 11); // Returns 'world'
   ```

5. **toUpperCase()**: Returns the calling string value converted to uppercase.
   ```javascript
   const str = "hello";
   const upper = str.toUpperCase(); // Returns 'HELLO'
   ```

6. **toLowerCase()**: Returns the calling string value converted to lowercase.
   ```javascript
   const str = "HELLO";
   const lower = str.toLowerCase(); // Returns 'hello'
   ```

7. **replace(searchValue, replaceValue)**: Returns a new string with some or all matches of a pattern replaced by a replacement.
   ```javascript
   const str = "Hello world";
   const newStr = str.replace("world", "everyone"); // Returns 'Hello everyone'
   ```

8. **substring(startIndex, endIndex)**: Returns the part of the string between the start and end indexes, or to the end of the string.
   ```javascript
   const str = "Hello world";
   const sub = str.substring(6); // Returns 'world'
   ```

9. **trim()**: Removes whitespace from both ends of a string.
   ```javascript
   const str = "   Hello world   ";
   const trimmed = str.trim(); // Returns 'Hello world'
   ```

These methods return new strings, leaving the original string unchanged because strings are immutable in JavaScript.

Even though strings are immutable in JavaScript, you can still "change" their value indirectly by reassigning the variable that holds the string to a new string value. For example:

```javascript
let str = "hello";
str = "world";
```

In this example, `str` initially holds the value `"hello"`, but then it is reassigned to `"world"`. While you can't directly modify the characters of a string, you can create a new string with the desired modifications and assign it to the variable.

```javascript
let str = "hello";
str[0] = "g"; // won't give error 
console.log(str); // print "hello"
```
 Trying to modify a string directly by accessing its characters like str[0] = "g" won't result in an error, but it also won't change the string. 
 
 This is because strings in JavaScript are immutable, meaning their individual characters cannot be changed after the string is created.