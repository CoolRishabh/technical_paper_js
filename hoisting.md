# Hoisting in JavaScript

Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their containing scope during the compilation phase, before the code is executed. This means that regardless of where variables and functions are declared within a scope, they are treated as if they were declared at the top of the scope.

However, it's important to note that only the declarations are hoisted, not the initializations. So, while the declaration is moved to the top, the assignment or initialization of the variable remains in its original place.

Here's an example to illustrate hoisting:

```javascript
console.log(x); // undefined
var x = 5;
console.log(x); // 5
```

In this example, `console.log(x)` is executed before `var x = 5;`, but it doesn't throw an error. This is because during the compilation phase, JavaScript moves the declaration `var x;` to the top of its containing scope, also for `var` it is hoisited to the top and is initilisedJ with  `undefined` value:

```javascript
var x;
console.log(x); // undefined
x = 5;
console.log(x); // 5
```

So, when `console.log(x)` is executed, `x` exists within its scope but hasn't been assigned a value yet, hence it logs `undefined`. Then, `x` is assigned the value `5`, and the second `console.log(x)` logs `5`. This behavior is an example of hoisting in JavaScript.