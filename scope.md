# Scope
In JavaScript, scopes define the visibility and accessibility of variables and functions.

There are mainly two types of scopes in JavaScript:

1. **Global Scope**: Variables declared outside of any function or block have global scope. They can be accessed from any part of the code, including within functions or blocks.

    ```javascript
    var globalVar = 10;

    function myFunction() {
        console.log(globalVar); // Accessible here
    }

    console.log(globalVar); // Accessible here
    ```

2. **Local Scope**: Variables declared within a function or block have local scope. They are only accessible within that function or block.

    ```javascript
    function myFunction() {
        let localVar = 20;
        console.log(localVar); // Accessible here
    }

    console.log(localVar); // Error: localVar is not defined
    ```

More detailed example:

```javascript
let globalVar = 10; // Global scope

function myFunction() {
    const localVar = 20; // Local scope to myFunction
    console.log(globalVar); // Accessible because it's in the global scope
    console.log(localVar); // Accessible because it's in the local scope
}

console.log(globalVar); // Accessible here
console.log(localVar); // Error: localVar is not defined
```

In the above example, `globalVar` is accessible both inside and outside the function `myFunction()` because it's declared in the global scope. However, `localVar` is only accessible inside `myFunction()` because it's declared in the local scope of that function. Trying to access `localVar` outside of `myFunction()` will result in an error.