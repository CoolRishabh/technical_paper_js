# Objects in JavaScript

1. **Objects**: An object is a collection of properties, where each property has a name (key) and a value associated with it. The value can be of any data type, including other objects, arrays, functions, etc. Objects in JavaScript can be created using object literals `{}`, constructor functions, or classes (introduced in ES6).

   Example of an object literal:
   ```javascript
   let person = {
       name: "Rishabh",
       age: 30,
       city: "New York"
   };
   ```

2. **Properties**: Properties are the key-value pairs within an object. You can access properties using dot notation (`object.property`) or bracket notation (`object['property']`).

   ```javascript
   console.log(person.name); // Output: Rishabh
   console.log(person['age']); // Output: 30
   ```

3. **Methods**: Methods are functions stored as object properties. They are used to perform actions or calculations using the data stored in the object. Methods can be defined within an object literal or added to an object later.

   ```javascript
   let person = {
       name: "Rishabh",
       age: 30,
       greet: function() {
           console.log("Hello, my name is " + this.name);
       }
   };

   person.greet(); // Output: Hello, my name is Rishabh
   ```

4. **Operations on Objects**:
   - **Object Creation**: Objects can be created using object literals, constructor functions, or ES6 classes.
   - **Property Access**: Properties can be accessed using dot notation or bracket notation.
   - **Property Assignment**: Properties can be added, modified, or removed from an object.
   - **Iteration**: You can loop through an object's properties using `for...in` loop or `Object.keys()`, `Object.values()`, and `Object.entries()` methods.
   - **Object Destruction**: Objects can be destructured to extract specific properties into variables.
